using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private CharacterController _controller = null;
    [SerializeField] private float _speed = 10f;

    private string _horizontal = "Horizontal";
    private string _vertical = "Vertical";
    
    private float _xMovement;
    private float _zMovement;

    private Vector3 _move;

    private void Update()
    {
        _xMovement = Input.GetAxis(_horizontal);
        _zMovement = Input.GetAxis(_vertical);

        _move = transform.right * _xMovement + transform.forward * _zMovement;
        _controller.Move(_move * _speed * Time.deltaTime);
    }
}