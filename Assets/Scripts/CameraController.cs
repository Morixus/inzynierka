using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _player = null;
    [SerializeField] private float _sensitivity;
    [SerializeField] private float _rotationLimit;

    private float _rotation = 0;
    private float _mouseHorizontal;
    private float _mouseVertical;

    private string _xMouse = "Mouse X";
    private string _yMouse = "Mouse Y";

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        _mouseHorizontal = Input.GetAxis(_xMouse) * _sensitivity * Time.deltaTime;
        _mouseVertical = Input.GetAxis(_yMouse) * _sensitivity * Time.deltaTime;

        _rotation -= _mouseVertical;
        _rotation = Mathf.Clamp(_rotation, -_rotationLimit, _rotationLimit);

        transform.localRotation = Quaternion.Euler(_rotation, 0f, 0f);
        _player.Rotate(Vector3.up * _mouseHorizontal);
    }
}