using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject _monster;
    [SerializeField] private Transform _monsterDest;

    private NavMeshAgent _monsterAgent;

    //Field of View
    public float radius;
    [Range(0f, 360f)]
    public float angle;
    public GameObject playerRef;
    public LayerMask _targetMask;
    public LayerMask _obstructionMask;
    public bool canSeePlayer;
    private float _delay = 0.2f;

    void Start()
    {
        _monsterAgent = GetComponent<NavMeshAgent>();
        StartCoroutine(FOVRoutine());
    }

    void Update()
    {
        if (canSeePlayer == true)
        {
            _monsterAgent.destination = _monsterDest.position;
        }
    }

    private IEnumerator FOVRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(_delay);
            FieldOfViewCheck();
        }
    }

    private void FieldOfViewCheck()
    {
        Collider[] rangeChecks = Physics.OverlapSphere(transform.position, radius, _targetMask);

        if (rangeChecks.Length != 0)
        {
            Transform target = rangeChecks[0].transform;
            Vector3 directionToTarget = (target.position - transform.position).normalized;

            if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, _obstructionMask))
                {
                    canSeePlayer = true;
                }
                else
                {
                    canSeePlayer = false;
                }
            }
            else
            {
                canSeePlayer = false;
            }
        }
        else if (canSeePlayer)
        {
            canSeePlayer = false;
        }
    }
}